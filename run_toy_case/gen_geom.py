
import pherosensor
from pheromone_dispersion.geom import MeshRect2D

"""
Script for generating the object of the class MeshRect2D.
The object is generated for the toy case from the data contained in the data folder.
This class is implemented in the PheroSensor-toolbox.
It contains the geometry of the spatio-temporal domain, as well as its discretization.
"""

def geom():
    Lx = 40 
    Ly = 60 
    Delta_x = .5
    Delta_y = .5 
    T_final = 20 
    msh = MeshRect2D(Lx, Ly, Delta_x, Delta_y, T_final)
    msh.calc_dt_implicit_solver(.1)
    return msh
