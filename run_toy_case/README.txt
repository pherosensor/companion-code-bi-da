This folder contains the scripts and data to run the toy case, as well as the obtained outputs and scripts to plot them.

- The data folder contains all the data needed to run the toy case.

- The gen_*.py scripts are used to generate the different object implemented in the PheroSensor-toolbox and needed to run the toy case. These object are generated from the data contained in the data folder. These scripts are automatically called in the scripts used to run the toy case.

- The run_direct_model.py and run_source_localization.py are used to solve resp. the pheromone propagation model and the inference problem for the estimation of the quantity of pheromone emitted. In both scripts, the outputs are saved in a folder whose path must be specified in the script.

- The ouput_* folders contain the output obtained from solving the pheromone propagation model and the inference problem with different regularization strategies.

- The compute_mono_sensor_adjoint_states.py enables to compute the mono-sensor adjoint states from the outputs contained in a folder whose path must be specified in the script.

- The plot folder contains all the notebooks to plot the ouputs of the pheromone propagation model and the inference problem with different regularization strategies.
