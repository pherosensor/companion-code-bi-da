import numpy as np
from pathlib import Path
import os
import pherosensor
from pheromone_dispersion.convection_diffusion_2D import Source
from source_localization.control import Control
from gen_geom import geom

"""
Script for generating the object of the class Control.
The object is generated for the toy case from the data contained in the data folder.
This class is implemented in the PheroSensor-toolbox.
It contains the control variable of the inference problem, that is the quantity of pheromone emitted by the insects.
"""

def ctrl():
    path_case = os.getcwd()
    path_data_toy_case = path_case + '/data'
    msh = geom()
    S_value = np.load(Path(path_data_toy_case) / 'S_background.npy')
    S = Source(msh, S_value, t=msh.t_array)

    exclusion_domain = np.full_like(
        S_value,
        False,
        dtype=bool
        )
    for ix, x in enumerate(msh.x):
        for iy, y in enumerate(msh.y):
            if y<=4. and y>=3.:
                exclusion_domain[:, iy, ix]=True
    exclusion_domain_reshape = exclusion_domain.reshape((exclusion_domain.size, ))
    
    return Control(S, msh, exclusion_domain=exclusion_domain_reshape, log_barrier_threshold=15.*1e-2)
