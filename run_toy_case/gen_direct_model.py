import numpy as np 
from pathlib import Path
import os

import pherosensor
from pheromone_dispersion.convection_diffusion_2D import DiffusionConvectionReaction2DEquation, Source
from pheromone_dispersion.diffusion_tensor import DiffusionTensor
from pheromone_dispersion.velocity import Velocity

from gen_geom import geom

"""
Script for generating the object of the class DiffusionConvectionReaction2DEquation.
The object is generated for the toy case from the data contained in the data folder.
This class is implemented in the PheroSensor-toolbox.
It contains the model of the pheromone propagation model.
"""

def direct_model():
    
    ### define paths
    path_case = os.getcwd()
    path_data_toy_case = path_case + '/data'

    ### define the mesh
    #print("generating the geometry")
    msh = geom()

    ### read the velocity data
    #print("generating the wind velocity field")
    U_vi = np.load(Path(path_data_toy_case) / 'U_at_vertical_interface.npy')
    U_hi = np.load(Path(path_data_toy_case) / 'U_at_horizontal_interface.npy')
    U = Velocity(msh, U_vi, U_hi, t=msh.t_array)

    ### define the diffusion tensor
    #print("generating the diffusion tensor")
    K = DiffusionTensor(U, 10, 10)

    #print("generating the deposition coefficient")
    deposition_coeff = np.load(Path(path_data_toy_case) / 'tau_loss.npy')

    #print("generating the source term")
    S_value = np.load(Path(path_data_toy_case) / 'S_target.npy')
    S_target = Source(msh, S_value, t = msh.t_array)

    #print("generating the PDE model")
    solver = 'implicit'
    EDP = DiffusionConvectionReaction2DEquation(U, K, deposition_coeff, S_target, msh, time_discretization=solver)

    return EDP 
