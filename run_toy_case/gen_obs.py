import numpy as np 
import os
from pathlib import Path

import pherosensor
from source_localization.obs import Obs

from gen_geom import geom

"""
Script for generating the object of the class Obs.
The object is generated for the toy case from the data contained in the data folder.
This class is implemented in the PheroSensor-toolbox.
It contains the observations, including the data and the observation operator.
"""

def obs():
    path_case = os.getcwd()
    path_data = path_case + '/data'
    t_obs = np.load(Path(path_data) / 't_obs.npy')
    X_obs = np.load(Path(path_data) / 'X_obs.npy')
    d_obs = np.load(Path(path_data) / 'd_obs_noised.npy')

    msh = geom()
    
    dt_sensor = 2.

    return Obs(t_obs, X_obs, d_obs, msh, dt_obs_operator=dt_sensor)
