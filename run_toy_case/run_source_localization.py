import os
import numpy as np
import time
import numpy.linalg as npl
import pherosensor
from source_localization.cost import Cost

from gen_geom import geom
from gen_obs import obs
from gen_ctrl import ctrl
from gen_adjoint_model import adjoint_model
from gen_direct_model import direct_model

"""
Script for solving the inference problem for the estimation of the quantity of pheromone emitted.
The ouputs are saved in the folder given by the path stored in the variable path_output_toy_case.
The path_output_toy_case can be changed according to the regularization strategy.
The regularization strategy can be change using the add_reg method of the cost object.
This method take as arguments resp. the name of the regularization term and the weight coefficient.
The j_obs_threshold argument of the minimize method should be uncomment so the optimization stops when the Morozov criteria is reached.
"""

path_case = os.getcwd()

t_0 = time.time()
direct_model = direct_model()
ctrl = ctrl()
obs = obs()
cost = Cost(obs.msh, obs, ctrl)
adjoint_model = adjoint_model()


##### Regularizations
# Uncomment regularization to add the corresponding regularization. If every regularization is commented, you get no-reg case (inference without regularization), see paper for additionnal insights.
#cost.add_reg("Stationnary population dynamic", 4. * 1e-4)
#cost.add_reg('Tikhonov', 10 * 1e-6)
#cost.add_reg('LASSO',  3 * 1e-4)

noise_level = np.load(path_case + '/data/noise_level.npy')
print("--- noise level = ", noise_level, " ---")

t_1 = time.time()

path_output_toy_case = (
    #path_case + "/output_regT"
    #path_case + "/output_regLASSO"
    #path_case + "/output_reggroupLASSO"
    path_case + "/output_noreg"
    #path_case + "/output_regPD"
    #path_case + "/output_regPDLASSO"
    #path_case + "/output_regLASSOT"
    #path_case + "/output_regPDT"
    #path_case + "/output_allreg"
)

options = {"ftol": 1e-16, "gtol": 1e-16, "nit_max": 1000  , "step size": 1 * 1.0e-0 } 
restart_flag = False #If False: restart a new inverse problem from scratch. If True: recover the last inference of a previous optimization, like in a hot start strategy.

print("")
print("--- minimization of the cost functions---")
print("step size = ",options["step size"])

if 'Stationnary population dynamic' in cost.alpha.keys():
    print("alpha_PD = ",cost.alpha['Stationnary population dynamic'])
if 'LASSO' in cost.alpha.keys():
    print("alpha_LASSO = ",cost.alpha['LASSO'])
if 'Tikhonov' in cost.alpha.keys():
    print("alpha_Tikhonov = ",cost.alpha['Tikhonov'])
direct_model, j_obs, j_reg, S_all_ite = cost.minimize(
    direct_model,
    adjoint_model,
    #"proximal gradient", # comment proximal gradient or gradient descent in case you have a differentiable or non-differentiable regularization.
    "gradient descent",
    restart_flag=restart_flag,
    options=options,
    path_save=path_output_toy_case,
    j_obs_threshold = noise_level
)

t_2 = time.time()
print("")
print("--- initialization in %s seconds ---" % (t_1 - t_0))
print("--- optimization of the cost function in %s seconds ---" % (t_2 - t_1))
