# README

This is the companion code for the paper « Biology-Informed inverse problems for insect pests detection using pheromone sensors » by Malou et al.

The main objective of this paper is to introduce a Biology-Informed Data-Assimilation (BI-DA) framework to solve the inverse problem of pheromone propagation, in order to build insect pest presence maps from signals obtained with pheromone sensors. In particular, we introduce Biology-informed regularization terms to bring biological prior knowledge to the data assimilation.

This archive contains all the script needed to run the simulations and to produce the figures of the paper.

## Installation of dependecies

### Install packages
First install the scripts dependencies. We assume you have a proper virtual environement with python=3.9 installed. First install packages (mainly for visualization).

```bash
pip install -r requirements.txt
```

### Install pherosensor package
Install the 'pherosensor' package

```bash
git clone https://forgemia.inra.fr/pherosensor/pherosensor-toolbox.git
cd pherosensor-toolbox-main
pip install .
```

Alternatively, you can obtain the sources from the Zenodo release of the toolbox at https://doi.org/10.5281/zenodo.11110047, and then

```bash
tar -zxvf pherosensor-toolbox.tar.gz
cd pherosensor-toolbox-main
pip install .
```

## Running the scripts

The scripts are organized into two different folders, corresponding to the two different test cases defined in Malou et al.: 'run_toy_case' and 'run_FAW_case'. We refer to Malou et al. for a precise definition of the different test cases. Breifly, the toy case correspond to a toy model specifically designed to benchmark the capabilities of BI-DA, whereas the FAW case represents a realistic case of Fall Army worm (FAW) settlement in an agricultural landscape.

Each folder contains its own README describing the folder content.

### Running the toy case

Get into the corresponding folder, then run the different codes to 1) solve the direct problem, 2) solve the inverse problem, 3) compute the mono-sensor adjoint state criteria, 4) compute the figures of the paper.

#### Run the direct model
In a terminal, type
```bash
python run_direct_model.py
```

#### Run the inverse problem
The script for BI-DA computation is stored in the 'run_source_localization.py' file. Please visit the file to select parameters. In particular, you can select the regularization, the step of the gradient descent, resolution methods ('gradient descent', or 'proximal gradient' if a lasso-type regularization is involved), the initial state of the optimization or the threshold for optimization convergence.

Note that to restart an optimization from a previous solution (e.g. during a hot start procedure), the previous solution must be completed until the convergence criteria is reached: the optimization state is only stored after a correct convergence of the optimization procedure.
```bash
python run_source_localization.py
```

#### Compute the mono-sensor adjoint state criteria
The mono-sensor adjoint state criteria can be obtained by runing
```bash
python compute_mono_sensor_adjoint_states.py
```

#### Compute the figures
The scripts needed to compute the figures are stored in the 'plot' folder in the form of different jupyter notebooks. Please run the different jupyter notebooks using as kernel the virtual environment where the dependencies are installed.



### Running the FAW case

Get into the corresponding folder, then run the different codes to 1) solve the direct problem, 2) solve the inverse problem, 3) compute the mono-sensor adjoint state criteria, 4) compute the figures of the paper. Please note that the direct problem must be computed at least once before computing the paper figures. In particular, the target source is computed during the direct problem computation.

#### Run the direct model
In a terminal, type
```bash
python run_direct_model.py
```

#### Run the inverse problem
The script for BI-DA computation is stored in the 'run_source_localization.py' file. Please visit the file to select parameters. In particular, you can select the regularization, the step of the gradient descent, resolution methods ('gradient descent', or 'proximal gradient' if a lasso-type regularization is involved), the initial state of the optimization or the threshold for optimization convergence. 

Note that to restart an optimization from a previous solution (e.g. during a hot start procedure), the previous solution must be completed until the convergence criteria is reached: the optimization state is only stored after a correct convergence of the optimization procedure.
```bash
python run_source_localization.py
```

#### Compute the mono-sensor adjoint state criteria
The mono-sensor adjoint state criteria can be obtained by runing
```bash
python compute_mono_sensor_adjoint_states.py
```

#### Compute the figures
The scripts needed to compute the figures are stored in the 'plot' folder in the form of different jupyter notebooks. Please run the different jupyter notebooks using as kernel the virtual environment where the dependencies are installed.

## Citation
If you use these scripts, please cite Malou et al., « Biology-Informed inverse problems for insect pests detection using pheromone sensors ».  

