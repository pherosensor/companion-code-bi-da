import numpy as np
import numpy.linalg as npl
from pathlib import Path
import sys 
import os

import pherosensor
from source_localization.cost import Cost

from gen_ctrl import ctrl
from gen_obs import obs
from gen_direct_model import direct_model
from gen_adjoint_model import adjoint_model

path_case = os.getcwd()
path_output_toy_case = (
    #path_case + "/output_noreg"
    #path_case + "/output_reggroupLASSO"
    path_case + "/output_regPD"
    #path_case + "/output_regTgroupLASSO"
    #path_case + "/output_allreg"
)
path_save = path_output_toy_case + '/one_sensor_adjoint_state'

traj_int_s = np.load(Path(path_output_toy_case) / 'traj_integrated_s.npy')
sum_step_size = np.load(Path(path_output_toy_case) / 'sum_step_size.npy')

direct_model = direct_model()
ctrl = ctrl()
obs = obs()
obs.d_obs *= sum_step_size
cost = Cost(obs.msh, obs, ctrl)
adjoint_model = adjoint_model()

cost.ctrl.value = np.copy(traj_int_s)
cost.ctrl.apply_control(direct_model)
print("--- solve the direct model for the trajectory-integrated source term ---")
direct_model.solver_est_at_obs_times(cost.obs, display_flag=False)
cost.obs.obs_operator()


global number_of_sensor_done
number_of_sensor_done = 0
onesensor_p = np.zeros((cost.obs.nb_sensors, cost.msh.t_array.size * cost.msh.x.size * cost.msh.y.size))

def process(i):

    def adj_der_obs_op(t, dc):
        return obs.onesensor_adjoint_derivative_obs_operator(t, dc, i)
    global number_of_sensor_done
    number_of_sensor_done += 1
    onesensor_p[i, :] = adjoint_model.solver(adj_der_obs_op, cost, display_flag=False)
    sys.stdout.write("mono-sensor adjoint model solved for %s sensors\n" % number_of_sensor_done)
    sys.stdout.flush()

print("--- starting the computation of the mono-sensors adjoint states ---")
for i in range(obs.nb_sensors): 
    process(i)

Linf_norm_one_sensor_adjoint_states = npl.norm(onesensor_p, axis=1, ord=np.inf)

np.save(Path(path_output_toy_case) / 'one_sensor_adjoint_states.npy', onesensor_p)
np.save(Path(path_output_toy_case) / 'linf_norm_one_sensor_adjoint_states.npy', Linf_norm_one_sensor_adjoint_states)
