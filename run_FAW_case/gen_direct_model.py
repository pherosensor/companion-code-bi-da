import numpy as np 
from pathlib import Path
import os

import pherosensor
from pheromone_dispersion.convection_diffusion_2D import DiffusionConvectionReaction2DEquation, Source
from pheromone_dispersion.diffusion_tensor import DiffusionTensor
from pheromone_dispersion.velocity import Velocity

from gen_geom import geom

"""
Script for generating the object of the class DiffusionConvectionReaction2DEquation.
The object is generated for the FAW case from the data contained in the data folder.
This class is implemented in the PheroSensor-toolbox.
It contains the model of the pheromone propagation model.
"""

def direct_model():
    
    ### define paths
    path_FAW_case = os.getcwd()
    path_data_FAW_case = path_FAW_case + '/data'

    ### define the mesh
    print("generating the geometry")
    msh = geom()

    ### read the velocity data
    print("generating the wind velocity field")
    U_vi = np.load(Path(path_data_FAW_case) / 'U_at_vertical_interface.npy')
    U_hi = np.load(Path(path_data_FAW_case) / 'U_at_horizontal_interface.npy')
    U = Velocity(msh, U_vi, U_hi)

    ### define the diffusion tensor
    print("generating the diffusion tensor")
    K = DiffusionTensor(U, 10, 10)

    print("generating the deposition coefficient")
    deposition_coeff = np.load(Path(path_data_FAW_case) / 'tau_loss.npy')

    print("generating the source term")
    if not (Path(path_data_FAW_case) / 'S.npy').exists():
        print("--- compute the target source term ---")
        with open('gen_source_term.py') as f:
            exec(f.read())
    S_value = np.load(Path(path_data_FAW_case) / 'S.npy')
    S_target = Source(msh, S_value, t = msh.t_array)

    print("generating the PDE model")
    solver = 'implicit with stationnary matrix inversion'
    tol = 1e-10
    EDP = DiffusionConvectionReaction2DEquation(U, K, deposition_coeff, S_target, msh, tol_inversion=tol, time_discretization=solver)
    
    fname = 'inv_matrix_implicit_solver.npy'
    if not (Path(path_data_FAW_case) / fname).exists():
        print("--- compute the inverse of the matrix of the implicit scheme ---")
        with open('compute_inv_matrix_implicit_solver.py') as f:
            exec(f.read())
    EDP.init_inverse_matrix(path_to_matrix=path_data_FAW_case, matrix_file_name=fname)
    return EDP 
