import numpy as np
import os
from pathlib import Path
from gen_geom import geom
import pherosensor
from pheromone_dispersion.convection_diffusion_2D import Source
from utils.plot_colormap import plot_colormap, plot_colormap_all_timestep

"""
Script for generating and storing the target estimate of the control variable.
This estimate is stored in the data folder.
"""

q = 2.7 # *1e-12

xc = 150 + np.min(msh.x)
yc = 150 + np.min(msh.y)
r = 10
xx, yy= np.meshgrid(msh.x, msh.y)
p_0 = np.zeros((msh.y.size, msh.x.size))
p_0[(xx-xc)**2 + (yy-yc)**2 < r**2] = 0.2

K = 0.1
lam = 0.05/60
t_s = 30*60
tau = -np.log( (2**(lam/K)-1) / (np.exp(lam*t_s)-2**(lam/K)) ) / lam
gamma = K / (1+np.exp(-lam*(msh.t_array-tau)))
proportion = np.array([np.exp(-np.sum(gamma[:i])*msh.dt) for i in range(msh.t_array.size)])

S_value = np.array([p_0*prop for prop in proportion])*q
path_case = os.getcwd()
path_data = path_case + '/data'
file_name = 'S.npy'
np.save(Path(path_save) / file_name, S_value)
