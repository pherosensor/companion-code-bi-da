from gen_direct_model import direct_model
import time
import os

"""
Script for solving the pheromone propagation model.
The ouputs are saved in the folder given by the path stored in the variable path_output_FAW_case.
"""

t_0 = time.time()
EDP = direct_model()
path_case = os.getcwd()
path_output_FAW_case = path_case + "/output_direct_model"
print("solving the PDE")
t_1 = time.time()
save_rate = 10*60//EDP.msh.dt
EDP.solver_save_all(path_save=path_output_FAW_case, save_rate=save_rate)
#c = EDP.solver()
t_2 = time.time()
print("")
print("--- initialization of the direct model in %s seconds ---" % (t_1 - t_0) )
print("--- resolution of the direct model in %s seconds ---" % (t_2 - t_1) )

