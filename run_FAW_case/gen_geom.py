
import pherosensor
from pheromone_dispersion.geom import MeshRect2D

"""
Script for generating the object of the class MeshRect2D.
The object is generated for the FAW case from the data contained in the data folder.
This class is implemented in the PheroSensor-toolbox.
It contains the geometry of the spatio-temporal domain, as well as its discretization.
"""

def geom():
    X_0 = (511500, 2305000) 
    Lx = 300 
    Ly = 400 
    Delta_x = 2. 
    Delta_y = 2. 
    T_final = 60*60*3 
    msh = MeshRect2D(Lx, Ly, Delta_x, Delta_y, T_final, X_0=X_0)
    msh.calc_dt_implicit_solver(30.)
    return msh

