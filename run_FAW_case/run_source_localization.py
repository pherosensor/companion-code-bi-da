import numpy as np 
import time
import os

import pherosensor
from source_localization.cost import Cost

from gen_geom import geom
from gen_obs import obs
from gen_ctrl import ctrl
from gen_adjoint_model import adjoint_model
from gen_direct_model import direct_model

"""
Script for solving the inference problem for the estimation of the quantity of pheromone emitted.
The ouputs are saved in the folder given by the path stored in the variable path_output_FAW_case.
The path_output_FAW_case can be changed according to the regularization strategy.
The regularization strategy can be change using the add_reg method of the cost object.
This method take as arguments resp. the name of the regularization term and the weight coefficient.
The j_obs_threshold argument of the minimize method should be uncomment so the optimization stops when the Morozov criteria is reached.
"""


path_case = os.getcwd()
t_0 = time.time()
print("")
print("--- Initialization of the direct model ---")
direct_model = direct_model()

print("--- Initialization of the control ---")
ctrl = ctrl()

print("--- Initialization of the obs ---")
obs = obs()

print("--- Initialization of the cost ---")
cost = Cost(obs.msh, obs, ctrl)

#cost.add_reg('LASSO', 1.*1e-1)
cost.add_reg('Population dynamic', 20.*1e8)
#cost.add_reg('Tikhonov', 4000.*1e1)

print("--- Initialization of the adjoint model ---")
adjoint_model = adjoint_model()

noise_level = np.load(path_case + '/data/noise_level.npy')
print('--- noise level = ', noise_level, ' ---')
t_1 = time.time()

path_output_FAW_case = (
    #path_case + "/output_noreg"
    #path_case + "/output_reggroupLASSO"
    path_case + "/output_regPD"
    #path_case + "/output_regTgroupLASSO"
    #path_case + "/output_allreg"
)
options={'ftol':1e-16,'gtol':1e-16,'nit_max':1200,"step size":1.*1.e-8} 
restart_flag = True

print("")
print("--- minimization of the cost function ---")
print("step size = ",options["step size"])
direct_model, j_obs, j_reg, S_all_ite = cost.minimize(
    direct_model,
    adjoint_model,
    "gradient descent",
    #"proximal gradient",
    restart_flag=restart_flag,
    options=options,
    path_save=path_output_FAW_case,
    #j_obs_threshold = noise_level
)

t_2 = time.time()
print("")
print("--- initialization in %s seconds ---" % (t_1 - t_0))
print("--- optimization of the cost function in %s seconds ---" % (t_2 - t_1))
