#from gen_direct_model import direct_model
import time
import scipy as sp
from gen_geom import geom
import numpy as np
from joblib import Parallel
from joblib import delayed
from pathlib import Path
import os

import pherosensor
from pheromone_dispersion.convection_diffusion_2D import DiffusionConvectionReaction2DEquation, Source
from pheromone_dispersion.diffusion_tensor import DiffusionTensor
from pheromone_dispersion.velocity import Velocity


msh = geom()

### define paths
path_FAW_case = os.getcwd()
path_data_FAW_case = path_FAW_case + '/data'

### read the velocity data
print("generating the wind velocity field")
U_vi = np.load(Path(path_data_FAW_case) / 'U_at_vertical_interface.npy')
U_hi = np.load(Path(path_data_FAW_case) / 'U_at_horizontal_interface.npy')
U = Velocity(msh, U_vi, U_hi)

### define the diffusion tensor
print("generating the diffusion tensor")
K = DiffusionTensor(U, 10, 10)

print("generating the deposition coefficient")
deposition_coeff = np.load(Path(path_data_FAW_case) / 'tau_loss.npy')

print("generating the source term")
S_value = np.load(Path(path_data_FAW_case) / 'S.npy')
S_target = Source(msh, S_value, t = msh.t_array)

print("generating the PDE model")
solver = 'implicit'
tol = 1e-10
EDP = DiffusionConvectionReaction2DEquation(U, K, deposition_coeff, S_target, msh, tol_inversion=tol, time_discretization=solver)

Identity = np.identity(EDP.msh.y.size * EDP.msh.x.size)
matrix_implicit_scheme = Identity + EDP.msh.dt * (
    -EDP.D._matmat(Identity) + EDP.R._matmat(Identity) + EDP.A._matmat(Identity)
)
inv_matrix_implicit_scheme = sp.linalg.inv(matrix_implicit_scheme)

matrix_file_name = f'inv_matrix_implicit_solver.npy'
np.save(Path(path_data_FAW_case) / matrix_file_name, inv_matrix_implicit_scheme) 
