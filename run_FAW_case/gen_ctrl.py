import numpy as np
from pathlib import Path
import os
import pherosensor
from pheromone_dispersion.convection_diffusion_2D import Source
from source_localization.control import Control
from gen_geom import geom

"""
Script for generating the object of the class Control.
The object is generated for the FAW case from the data contained in the data folder.
This class is implemented in the PheroSensor-toolbox.
It contains the control variable of the inference problem, that is the quantity of pheromone emitted by the insects.
"""

def ctrl():
    path_case = os.getcwd()
    path_data_FAW_case = path_case + '/data'
    msh = geom()
    
    if not (Path(path_data_FAW_case) / 'S_background.npy').exists():
        print("--- compute the backgroun source term ---")
        with open('gen_background_source_term.py') as f:
    S_value = np.load(Path(path_data_FAW_case) / 'S_background.npy')
    S = Source(msh, S_value, t=msh.t_array)

    q = 2.7
    xc = 150 + np.min(msh.x)
    yc = 150 + np.min(msh.y)
    r = 10
    xx, yy= np.meshgrid(msh.x, msh.y)
    p_0 = np.zeros((msh.y.size, msh.x.size))
    p_0[(xx-xc)**2 + (yy-yc)**2 < r**2] = 0.2
    K = 0.1
    lam = 0.05/60
    t_s = 30*60
    tau = -np.log( (2**(lam/K)-1) / (np.exp(lam*t_s)-2**(lam/K)) ) / lam
    gamma = K / (1+np.exp(-lam*(msh.t_array-tau)))
    gamma_tile = np.tile(gamma[:,None,None],(1,msh.y.size,msh.x.size))

    ctrl = Control(S, msh, population_dynamique_death_rate=gamma_tile)
    ctrl.set_C_inv_to_id_on_initial_time(msh)

    return ctrl
